package com.example.asynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    URL ImageUrl = null;
    InputStream is = null;
    Bitmap bmImg = null;
    ImageView imageView = null;
    Button button = null;
    ProgressDialog p;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.asyncTask);
        imageView = findViewById(R.id.image);

        //Deprecated since API 30
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AsyncTaskExample asyncTask = new AsyncTaskExample();
//                asyncTask.execute("https://stis.edupage.org/photos/partners/thumbs/max1920x1920trifxc147ae20bab025a2_eduresized_logo-stis.png ");
//            }
//        });
        button.setOnClickListener(view -> {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                // onPreExecute()
                runOnUiThread(() -> {
                    p = new ProgressDialog(MainActivity.this);
                    p.setMessage("Downloading...");
                    p.setIndeterminate(false);
                    p.setCancelable(false);
                    p.show();
                });

                // doInBackground
                try {
                    ImageUrl = new URL("https://stis.edupage.org/photos/partners/thumbs/max1920x1920trifxc147ae20bab025a2_eduresized_logo-stis.png");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) ImageUrl.openConnection();
                    httpURLConnection.connect();
                    bmImg = BitmapFactory.decodeStream(httpURLConnection.getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // onPostExecute
                runOnUiThread(() -> {
                    p.dismiss();
                    imageView.setImageBitmap(bmImg);
                });
            });
        });

    }

    private class AsyncTaskExample extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            p = new ProgressDialog(MainActivity.this);
            p.setMessage("Downloading...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }
        @Override
        protected Bitmap doInBackground(String...
                                                strings) {
            try {
                ImageUrl = new URL(strings[0]);
                HttpURLConnection conn = (HttpURLConnection) ImageUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();is = conn.getInputStream();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                bmImg = BitmapFactory.decodeStream(is,null, options);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmImg;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (imageView != null) {
                p.hide();
                imageView.setImageBitmap(bitmap);
            } else {
                p.show();
            }
        }
    }
}